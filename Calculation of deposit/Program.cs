﻿using System;

namespace testClassesAndMethods
{

    class Depositor
    {
        public bool ChecksDepositorAge(int age)
        {
            if (age < 18)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    class Deposit
    {
        public double CalculateFinalDepositStandart(double userCash, int yearsOfDeposit, double depositPercent)
        {
            for (int i = 0; i < yearsOfDeposit; i++)
            {
                var yearPercent = userCash * depositPercent / 100;
                userCash += yearPercent;
                userCash = Math.Round(userCash, 2, MidpointRounding.ToEven);
            }
            return userCash;
        }

        public double CalculateFinalDepositWithCapitaliatoin(double userCash, int yearsOfDeposit, double depositPercent)
        {
            for (int i = 0; i < yearsOfDeposit * 12; i++)
            {
                var monthPercent = depositPercent / 12;
                var monthProfit = userCash * monthPercent / 100;
                userCash += monthProfit;
                userCash = Math.Round(userCash, 2, MidpointRounding.ToEven);
            }
            return userCash;
        }

        public double ChooseDepositPercent(int yearsOfDeposit)
        {
            if (yearsOfDeposit <= 3)
            {
                return 5.5;
            }
            else if (yearsOfDeposit > 3)
            {
                return 6.5;
            }
            return 0;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var Depositor = new Depositor();

            while (true)
            {
                Console.WriteLine("Enter your age:");
                var ageString = Console.ReadLine();
                var isAgeStringValid = int.TryParse(ageString, out int age);

                if (isAgeStringValid)
                {
                    var result = Depositor.ChecksDepositorAge(age);

                    if (result)
                    {
                        Console.WriteLine("You can make a deposit \r\n");
                    }
                    else
                    {
                        Console.WriteLine($"You can't make a deposit, please, wait: {18 - age} years \r\n");
                        return;
                    }
                }
                else
                {
                    Console.WriteLine("Input error, please, try again ");
                }

                Console.WriteLine("Enter deposit amount:");
                var userDepositAmountString = Console.ReadLine();
                var userDepositAmount = double.Parse(userDepositAmountString);

                Console.WriteLine("Enter deposit tern:");
                var depositDurationString = Console.ReadLine();
                var depositDuration = int.Parse(depositDurationString);
                var deposiPercent = new Deposit().ChooseDepositPercent(depositDuration);

                Console.WriteLine("Choose type of deposit. Enter '1' - with capitalization, '2' - without capitalization");
                var typeOfDepositString = Console.ReadLine();
                var typeOfDeposit = int.Parse(typeOfDepositString);
                if (typeOfDeposit == 1)
                {
                    var finalDeposit = new Deposit().CalculateFinalDepositWithCapitaliatoin(userDepositAmount, depositDuration, deposiPercent);
                    Console.WriteLine($"Your percent rate: {deposiPercent}. After { depositDuration } year, you deposit will be: { finalDeposit } \r\n");
                }
                else if (typeOfDeposit == 2)
                {
                    var finalDeposit = new Deposit().CalculateFinalDepositStandart(userDepositAmount, depositDuration, deposiPercent);
                    Console.WriteLine($"Your percent rate: {deposiPercent}. After { depositDuration } year, you deposit will be: { finalDeposit } \r\n");
                }
            }
        }
    }
}
